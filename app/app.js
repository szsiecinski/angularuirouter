'use strict';
var routerApp = angular.module('routerApp', ['ui.router']);

routerApp.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/home');

    $stateProvider

        // HOME STATES AND NESTED VIEWS ========================================
        .state('home', {
            url: '/',
            templateUrl: 'home/home.html',
            controller: 'HomeCtrl'
        })

        // ABOUT PAGE AND MULTIPLE NAMED VIEWS =================================
        .state('about', {
            // we'll get to this in a bit
            url: '/about',
            template: '<h2>About</h2><p>bbb</p>'
        })

        .state('contact', {
          url: '/contact',
          template: '<h2>Contact</h2><p>ccc</p>'
        })

        .state('home.superheros', {
          url: 'home/superheros',
          controller: 'HomeCtrl',
          templateUrl: 'home/superheros.html'
        })

        .state('home.voojitsu', {
          url: 'home/voojitsu',
          template: '<p>Voojitsu!</p>'
        });
});
