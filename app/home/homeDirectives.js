'use strict';
var app = angular.module('routerApp');

app.directive('superheros', function() {
  return {
    restrict: 'E',
    controller: 'HomeCtrl',
    link: function(scope) {
      '<ul><li ng-repeat="superbohater in scope.superbohaterowie">{{superbohater}}</li></ul>'
  }
});

app.directive('voojitsu', function() {
  return {
    restrict: 'E',
    controller: 'HomeCtrl',
    link: function(scope) {
      '<p>{{scope.voojitsu()}}</p>'
      }
  }
});
